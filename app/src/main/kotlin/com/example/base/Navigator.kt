package com.example.base

import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import com.example.repository.data_sources.LocalDataSource
import com.example.repository.data_sources.SharedPrefsDataSource
import com.example.ui.awsdemo.activity.AWSDemoActivity
import com.example.ui.awsdemo.activity.AWSDetailActivity
import com.example.utils.GlobalConstants
import javax.inject.Inject

/**
 * Created by hunain.liaquat.
 */
class Navigator @Inject constructor(
    var localDataSource: LocalDataSource,
    sharedPrefDataSource: SharedPrefsDataSource
) {
    private fun changeFragment(
        activity: BaseActivity,
        fragment: BaseFragment,
        addToStack: Boolean,
        frameId: Int,
        tag: String
    ) {
        activity.replaceFragment(fragment, addToStack, frameId, tag)
    }

    private fun startActivity(activity: BaseActivity, intent: Intent) {
        activity.startActivity(intent)
    }

    private fun startActivityForResult(activity: BaseActivity, intent: Intent, requestCode: Int) {
        activity.startActivityForResult(intent, requestCode)
    }

    fun gotoPlayStore(activity: BaseActivity) {
        val uriBuilder =
            Uri.parse("https://play.google.com/store/apps/details?id=com.languagedrops.drops.international")
                .buildUpon()
                .appendQueryParameter("launch", "true")
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = uriBuilder.build()
        }
        activity.startActivity(intent)
        activity.finish()
    }

    fun gotoAWSDemoActivity(activity: BaseActivity, bundle: Bundle? = null) {
        val intent = Intent(activity, AWSDemoActivity::class.java)
        if (bundle != null)
            intent.putExtras(bundle)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(activity, intent)
    }

    fun gotoNotificationsDetailsActivity(activity: BaseActivity, bundle: Bundle) {
        val intent = Intent(activity, AWSDetailActivity::class.java)
        intent.putExtras(bundle)
        activity.startActivity(intent)
    }


}