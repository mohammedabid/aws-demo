package com.example.base

import android.annotation.TargetApi
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.androidadvance.topsnackbar.TSnackbar
import com.example.R
import com.example.databinding.ActivityBaseBinding
import com.example.repository.data_sources.SharedPrefsDataSource
import com.example.utils.GlobalConstants.LANG_AR
import com.example.utils.GlobalConstants.LANG_EN
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.HasSupportFragmentInjector
import java.util.*
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
    private lateinit var activityBaseBinding: ActivityBaseBinding
    lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView
    @Inject
    open lateinit var sharedPrefsDataSource: SharedPrefsDataSource
    @Inject
    open lateinit var navigator: Navigator


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base)
//        setToolbar(R.layout.toolbar)
        setStartLanguage()
    }

    protected fun SetBackgroundColor(color: Int) {
        activityBaseBinding.rlMain.setBackgroundColor(color)
    }

    protected fun SetBackgroundResource(drawable: Int) {
        activityBaseBinding.rlMain.setBackgroundResource(drawable)
    }

    open fun replaceFragment(
        fragment: BaseFragment,
        addToBackStack: Boolean,
        frameId: Int,
        tag: String
    ) {
        if (supportFragmentManager != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            if (fragment != null)
                fragmentTransaction.replace(frameId, fragment, tag)
            if (addToBackStack)
                fragmentTransaction.addToBackStack(fragment!!.javaClass.simpleName)
            if (!isFinishing) {
                fragmentTransaction.commit()
            }

        }
    }

    fun changeLanguage() {
        if (sharedPrefsDataSource.currLang.isNotEmpty() && sharedPrefsDataSource.currLang.equals(
                LANG_EN,
                ignoreCase = true
            )
        ) {
            sharedPrefsDataSource.currLang = LANG_AR
        } else {
            sharedPrefsDataSource.currLang = LANG_EN
        }
        setLanguage()
    }

    fun setLanguage(lang : String){
        sharedPrefsDataSource.currLang = lang
        setLanguage()
    }
    fun setLanguage() {
        setLocale(sharedPrefsDataSource.currLang)
        val activity = this
        activity.startActivity(activity.intent)
        activity.finish()
    }

    fun setStartLanguage() {
        try {
            val local = Locale.getDefault().language
            if (sharedPrefsDataSource.currLang.isEmpty()) {
                if (local != null && local.equals(LANG_AR, ignoreCase = true))
                    sharedPrefsDataSource.currLang = LANG_AR
                else
                    sharedPrefsDataSource.currLang = LANG_EN
            }
            setLocale(sharedPrefsDataSource.currLang)
        } catch (e: Exception) {
        }

    }

    open fun removeFragment(tag: String) {
        if (supportFragmentManager != null) {
            val fragment = supportFragmentManager.findFragmentByTag(tag)
            if (fragment != null)
                supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
    }

    /* Override all setContentView methods to put the content view to the FrameLayout view_stub
* so that, we can make other activity implementations looks like normal activity subclasses.
*/
    override fun setContentView(layoutResID: Int) {
        if (this::activityBaseBinding.isInitialized) {
            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val lp = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            val stubView = inflater.inflate(layoutResID, activityBaseBinding.viewStub, false)
            activityBaseBinding.viewStub.removeAllViews()
            activityBaseBinding.viewStub.addView(stubView, lp)
        } else {
            super.setContentView(layoutResID)
        }
    }

    override fun setContentView(view: View) {
        if (this::activityBaseBinding.isInitialized) {
            val lp = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            activityBaseBinding.viewStub.removeAllViews()
            activityBaseBinding.viewStub.addView(view, lp)
        }
    }

    override fun setContentView(view: View, params: ViewGroup.LayoutParams) {
        if (this::activityBaseBinding.isInitialized) {
            activityBaseBinding.viewStub.removeAllViews()
            activityBaseBinding.viewStub.addView(view, params)
        }
    }


    open fun setToolbar(layoutId: Int) {
        setToolbar(layoutId, true)
    }

    open fun setToolbar(layoutId: Int, showBackButton: Boolean) {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val lp = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val stubView = inflater.inflate(layoutId, activityBaseBinding.flToolbar, false)
        setToolbar(stubView, lp, showBackButton)
    }

    open fun setToolbar(stubView: View, lp: ViewGroup.LayoutParams) {
        setToolbar(stubView, lp, true)
    }

    open fun setToolbar(stubView: View, lp: ViewGroup.LayoutParams, showBackButton: Boolean) {
        activityBaseBinding.flToolbar.removeAllViews()
        activityBaseBinding.flToolbar.addView(stubView, lp)
        activityBaseBinding.flToolbar.visibility = View.VISIBLE
        toolbar = stubView.findViewById(R.id.toolbar)
        toolbarTitle = stubView.findViewById(R.id.toolbar_title)
        if (toolbar == null || toolbarTitle == null)
            throw Exception("Please provide toolbar and toolbarTitle in the xml layout file")

        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.title = ""
        }
        if (showBackButton)
            setBackButtonDrawable(R.drawable.abc_ic_ab_back_material)
    }

    open fun showToolbar(showToolbar: Boolean) {
        activityBaseBinding.flToolbar.visibility = if (showToolbar) View.VISIBLE else View.GONE
    }

    open fun setBackButtonDrawable(backButtonDrawable: Int) {
        showBackButton(true)
        toolbar!!.setNavigationIcon(backButtonDrawable)
        toolbar!!.setNavigationOnClickListener {
            handleBackPressed()
        }
    }

    private fun handleBackPressed() {
        if (activityBaseBinding.flProgress.visibility == View.VISIBLE) {
            showPageLoader(false)
        } else {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        if (activityBaseBinding.flProgress.visibility == View.VISIBLE) {
            showPageLoader(false)
        } else
            super.onBackPressed()
    }

    open fun isLoading(): Boolean {
        return activityBaseBinding.flProgress.visibility == View.VISIBLE
    }

    open fun setToolbarTitle(title: String) {
        toolbarTitle?.let {
            toolbarTitle!!.text = title
        }
    }


    open fun showBackButton(showBackButton: Boolean) {
        if (!showBackButton) {
            toolbar!!.navigationIcon = null
            supportActionBar?.setIcon(null)
        }
        supportActionBar?.setHomeButtonEnabled(showBackButton)
        supportActionBar?.setDisplayHomeAsUpEnabled(showBackButton)
        supportActionBar?.setDisplayHomeAsUpEnabled(showBackButton)
        actionBar?.setHomeButtonEnabled(showBackButton)
        actionBar?.setDisplayHomeAsUpEnabled(showBackButton)
        actionBar?.setDisplayHomeAsUpEnabled(showBackButton)
    }

    open fun showPageLoader() {
        showPageLoader(show = true, hideBackground = false)
    }

    open fun showPageLoader(show: Boolean) {
        showPageLoader(show, true)
    }

    open fun showPageLoader(show: Boolean, hideBackground: Boolean) {
        if (show) {
            activityBaseBinding.flProgress.visibility = View.VISIBLE
            //   showToolbar(false)
            if (hideBackground) {
                activityBaseBinding.viewStub.visibility = View.GONE
            }
        } else {
            activityBaseBinding.flProgress.visibility = View.GONE
            activityBaseBinding.viewStub.visibility = View.VISIBLE
            //  showToolbar(true)
        }
    }

    open fun onResponse() {
    }


    //Set locale
    private fun setLocale(language: String) {
        val locale = Locale(language)
        val resources = resources
        val configuration = resources.configuration
        val displayMetrics = resources.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale)
        } else {
            configuration.locale = locale
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            applicationContext.createConfigurationContext(configuration)
        } else {
            resources.updateConfiguration(configuration, displayMetrics)
        }

    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(updateBaseContextLocale(base))
    }

    private fun updateBaseContextLocale(context: Context): Context {
        if (!this::sharedPrefsDataSource.isInitialized) {
            sharedPrefsDataSource = SharedPrefsDataSource(context)
        }
        val language =
            sharedPrefsDataSource.currLang // Helper method to get saved language from SharedPreferences
        val locale = Locale(language)
        Locale.setDefault(locale)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResourcesLocale(context, locale)
        } else updateResourcesLocaleLegacy(context, locale)

    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResourcesLocale(context: Context, locale: Locale): Context {
        val configuration = context.resources.configuration
        configuration.setLocale(locale)
        return context.createConfigurationContext(configuration)
    }

    private fun updateResourcesLocaleLegacy(context: Context, locale: Locale): Context {
        val resources = context.resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
        return context
    }


    fun showSnackbar(text: String, iconId: Int, iconSize: Float, duration: Int, margin: Int) {
        var snackbar = TSnackbar.make(findViewById(android.R.id.content), text, duration)
        snackbar.setIconLeft(iconId, iconSize)
        snackbar.setIconPadding(20)
        val view = snackbar.view
        if (margin > 0) {
            val params = view.layoutParams as FrameLayout.LayoutParams
            params.setMargins(margin, 0, margin, 0)
            view.layoutParams = params
        }
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        var textView = view.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)
        textView.maxLines = 5
        textView.setTextColor(ContextCompat.getColor(this, R.color.dark_text_color))
        snackbar.show()
    }


}