package com.example.base

import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestOptions
import com.example.utils.GlobalConstants
import java.text.SimpleDateFormat
import javax.inject.Inject


class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {
    @BindingAdapter("imageUrl")
    fun bindImage(imageView: ImageView, url: String?) {

        val customHeaderUrl = GlideUrl(
            url, LazyHeaders.Builder()
                .addHeader("custom_header", "pre_prod")
                .build()
        )

        val requestOptions = RequestOptions()
//                .fallback(R.drawable.default_image)
//                .error(R.drawable.default_image)
//                .placeholder(R.drawable.default_image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
        Glide.with(fragment).setDefaultRequestOptions(requestOptions).load(customHeaderUrl)
            .into(imageView)
    }

    @BindingAdapter(value = ["imageUrl", "defaultImage"], requireAll = false)
    fun bindImage(imageView: ImageView, url: String?, defaultImage: Drawable? = null) {

        var customHeaderUrl: GlideUrl? = null
        /*if (url != null && url.startsWith("http")) {
            customHeaderUrl = GlideUrl(
                url, LazyHeaders.Builder()
                    .addHeader("custom_header", "pre_prod")
                    .build()
            )
        }*/
        val requestOptions = RequestOptions()
//                .fallback(R.drawable.default_image)
//                .error(R.drawable.default_image)
//            .placeholder(if (defaultImage != null) defaultImage else R.drawable.avatar_photo)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
        if (defaultImage != null)
            requestOptions.placeholder(defaultImage)
        if (customHeaderUrl != null) {
            Glide.with(fragment).setDefaultRequestOptions(requestOptions).load(customHeaderUrl)
                .into(imageView)
        } else {
            Glide.with(fragment).setDefaultRequestOptions(requestOptions).load(url)
                .into(imageView)
        }
    }

    @BindingAdapter("imageResource")
    fun bindImageResource(imageView: ImageView, resourceId: Int?) {
        val requestOptions = RequestOptions()
//                .fallback(R.drawable.default_image)
//                .error(R.drawable.default_image)
//                .placeholder(R.drawable.default_image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
//        Glide.with(fragment).setDefaultRequestOptions(requestOptions).load(resourceId)
//            .into(imageView)
        imageView.setImageResource(resourceId!!)
    }


    @BindingAdapter(value = ["date", "format"], requireAll = false)
    fun bindDate(textView: TextView, dateStr: String?, format: String?) {
        var simpleDateFormat = SimpleDateFormat(GlobalConstants.SERVER_DATE_FORMAT)
        var date = simpleDateFormat.parse(dateStr)

        if(dateStr != null && dateStr.isNotEmpty()){
            var simpleDateFormat2 = SimpleDateFormat(GlobalConstants.DATE_FORMAT)
             textView.text = (simpleDateFormat2.format(date))
        }
    }

}