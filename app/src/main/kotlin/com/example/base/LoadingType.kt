package com.example.base

enum class LoadingType {

    FULL_LOADING,
    INLINE_LOADING,
    TAB_LOADING,
    PAGE_LOADING,
}