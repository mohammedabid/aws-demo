package com.example.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.R
import com.example.injection.Injectable
import com.example.utils.Dialogs

abstract class BaseFragment : Fragment(), Injectable {

    protected var rootView: View? = null
    private var baseViewModel: BaseViewModel? = null

    abstract fun initUI()

    open fun showProgressDialog(show: Boolean) {
        if (activity is BaseActivity) {
            val baseActivity = activity as BaseActivity
            baseActivity.showPageLoader(show)
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseViewModel = initViewModel()
        initUI()
        if (baseViewModel != null) {
            baseViewModel?.stateLiveData?.observe(this, Observer { state ->
                when (state?.apiState) {
                    ApiState.SHOW_LOADER -> {
                        when (state.data) {
                            LoadingType.PAGE_LOADING -> {
                                showPageLoader()
                            }
                            LoadingType.FULL_LOADING -> {
                                showFullLoader()
                            }
                            LoadingType.INLINE_LOADING -> {
                                showInlineLoader()
                            }
                            LoadingType.TAB_LOADING -> {
                                showTabLoader()
                            }
                        }

                    }
                    ApiState.HIDE_LOADER -> {
                        when (state.data) {
                            LoadingType.PAGE_LOADING -> {
                                hidePageLoader()
                            }
                            LoadingType.FULL_LOADING -> {
                                hideFullLoader()
                            }
                            LoadingType.INLINE_LOADING -> {
                                hideInlineLoader()
                            }
                            LoadingType.TAB_LOADING -> {
                                hideTabLoader()
                            }
                        }

                    }
                    ApiState.NETWORK_ERROR -> {

                    }
                    ApiState.GENERAL_ERROR -> {

                    }
                    ApiState.ERROR -> {

                    }
                    ApiState.SESSION_EXPIRED -> {

                    }
                    else -> {

                    }
                }
            })
            baseViewModel?.errorLiveData?.removeObservers(this)
            baseViewModel?.errorLiveData?.observe(this, Observer {
                onError(getString(R.string.error), it, getString(R.string.ok), true, Runnable {
                })
            })
        }

    }


    private fun hideTabLoader() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun hideInlineLoader() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun hideFullLoader() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun hidePageLoader() {
        (activity as BaseActivity).showPageLoader(false)
    }

    open fun showTabLoader() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    open fun showInlineLoader() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    open fun showPageLoader() {
        (activity as BaseActivity).showPageLoader(show = true)
    }

    open fun showFullLoader() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    open fun onResponse() {
        if (activity is BaseActivity) {
            val baseActivity = activity as BaseActivity
            baseActivity.onResponse()
        }
    }

    open fun onError(title: String, message: String, positiveText: String, cancelable: Boolean, runnable: Runnable) {

    }

    open fun onGeneralError(message: String) {

    }

    abstract fun initViewModel(): BaseViewModel?

}