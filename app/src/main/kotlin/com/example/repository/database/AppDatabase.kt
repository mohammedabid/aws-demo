package com.example.repository.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.model.awsdemo.AWSResult
import com.example.model.awsdemo.Post

@Database(entities = [Post::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
//    abstract fun postDao(): PostDao
}