package com.example.repository

import com.example.model.awsdemo.GetAWSDemoResponseModel
import com.example.model.base.BaseRequestModel
import com.example.model.base.BaseResponseModel
import com.example.utils.ServiceNames
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

/**
 * The interface which provides methods to get result of webservices
 */


interface ApiInterface {
    /**
     * Get the list of the pots from the API
     */

    @GET("dynamodb-writer")
    fun getAWSDemo(): Observable<GetAWSDemoResponseModel>


}