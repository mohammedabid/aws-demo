package com.example.repository.repos

import com.example.repository.data_sources.LocalDataSource
import com.example.repository.data_sources.NetworkDataSource
import com.example.repository.data_sources.SharedPrefsDataSource
import javax.inject.Inject

class AuthRepo @Inject constructor(var networkDataSource: NetworkDataSource,
                                   var localDataSource: LocalDataSource,
                                   var sharedPrefsDataSource: SharedPrefsDataSource
) {

}