package com.example.repository.repos

import android.util.Log
import com.example.model.awsdemo.GetAWSDemoResponseModel
import com.example.model.base.BaseRequestModel
import com.example.model.base.BaseResponseModel
import com.example.repository.data_sources.BaseDataSource
import com.example.repository.data_sources.LocalDataSource
import com.example.repository.data_sources.SharedPrefsDataSource
import io.reactivex.Observable
import javax.inject.Inject

class ContentRepo @Inject constructor(
    private var networkDataSource: BaseDataSource,
    private var sharedPrefsDataSource: SharedPrefsDataSource,
    private var baseRequestModel: BaseRequestModel
) {

    fun getAWSDemo(): Observable<GetAWSDemoResponseModel> {
        return networkDataSource.getAWSDemo()
    }

}