package com.example.repository.data_sources

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object PreferenceKeys {
    const val PREF_UNIQUE_ID = "unique_id"
    const val PREFS_FILENAME = "com.example.prefs"
    const val CURR_LANG = "currLang"
}

class SharedPrefsDataSource(context: Context) {
    private val prefs: SharedPreferences = context.getSharedPreferences(PreferenceKeys.PREFS_FILENAME, 0)
    private var gson = Gson()

    inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)

    var currLang: String
        get() = prefs.getString(PreferenceKeys.CURR_LANG, "")!!
        set(value) = prefs.edit().putString(PreferenceKeys.CURR_LANG, value).apply()

    fun clear() {
        prefs.edit().clear()
    }
}