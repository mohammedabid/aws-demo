package com.example.repository.data_sources

import com.example.model.awsdemo.GetAWSDemoResponseModel
import com.example.model.base.BaseRequestModel
import com.example.model.base.BaseResponseModel
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface BaseDataSource {
    fun getAWSDemo(): Observable<GetAWSDemoResponseModel>
}