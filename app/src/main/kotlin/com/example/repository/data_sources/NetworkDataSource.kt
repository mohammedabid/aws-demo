package com.example.repository.data_sources

import com.example.model.awsdemo.GetAWSDemoResponseModel
import com.example.model.base.BaseRequestModel
import com.example.model.base.BaseResponseModel
import com.example.repository.ApiInterface
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class NetworkDataSource @Inject constructor(var apiInterface: ApiInterface) :
    BaseDataSource {

    override fun getAWSDemo(): Observable<GetAWSDemoResponseModel> {
        return apiInterface.getAWSDemo()
    }

}