package com.example.utils;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;
import android.util.Log;

import java.io.*;

/**
 * Created by El-Tohamy on 13/02/2017.
 */

public class ImageUtil {

    public static final int PIC_FROM_CAMERA = 0;
    public static final int PIC_FROM_GALLERY = 1;
    private static long GALLERY_OPENED = 0;

    public static String analyzePhotoPathFromLocalUri(Context mContext, Uri imageUri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = mContext.getContentResolver().query(imageUri, filePathColumn, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            return picturePath;
        } else
            return imageUri.getPath();
    }

    public static Bitmap decodeUri(Context mContext, Uri selectedImage) {
        try {

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            try {
                BitmapFactory.decodeStream(
                        mContext.getContentResolver().openInputStream(selectedImage), null, o);
                final int REQUIRED_SIZE = 500;
                int width_tmp = o.outWidth, height_tmp = o.outHeight;
                int scale = 1;
                while (true) {
                    if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                        break;
                    }
                    width_tmp /= 2;
                    height_tmp /= 2;
                    scale *= 2;
                }

                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                return BitmapFactory.decodeStream(
                        mContext.getContentResolver().openInputStream(selectedImage), null, o2);
            } catch (FileNotFoundException e) {
                return null;
            }
        } catch (OutOfMemoryError outOfMemoryError) {
            return null;
        }
    }

    private static int getUndefinedRotation(Context context, String filePath) {
        int rotation = -1;
        long fileSize = new File(filePath).length();

        Cursor mediaCursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.ImageColumns.ORIENTATION, MediaStore.MediaColumns.SIZE}, MediaStore.MediaColumns.DATE_ADDED + ">=?", new String[]{String.valueOf(GALLERY_OPENED / 1000 - 1)}, MediaStore.MediaColumns.DATE_ADDED + " desc");

        if (mediaCursor != null && GALLERY_OPENED != 0 && mediaCursor.getCount() != 0) {
            while (mediaCursor.moveToNext()) {
                long size = mediaCursor.getLong(1);
                //Extra check to make sure that we are getting the orientation from the proper file
                if (size == fileSize) {
                    return mediaCursor.getInt(0);
                }
            }
        } else {
            return 0;
        }
        return -1;
    }

    public static void setOpenedTime(long timeInMillis) {
        GALLERY_OPENED = timeInMillis;
    }

    public static Bitmap rotateBitmap(Context mContext, Bitmap bitmap, Uri uri, int PIC_FROM) {
        String filepath = null;
        if (PIC_FROM == PIC_FROM_GALLERY) {
            filepath = analyzePhotoPathFromLocalUri(mContext, uri);
        } else if (PIC_FROM == PIC_FROM_CAMERA) {
            filepath = uri.getPath();
        }
        Bitmap rotatedBitmap = null;
        Matrix matrix = new Matrix();
        try {
            ExifInterface exif = new ExifInterface(filepath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//            if (orientation == ExifInterface.ORIENTATION_UNDEFINED || orientation == -1) {
//                orientation = getUndefinedRotation(mContext, filepath);
//            }
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.postRotate(90);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.postRotate(180);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.postRotate(270);
            } else if (orientation == ExifInterface.ORIENTATION_FLIP_HORIZONTAL) {
                matrix.preScale(-1, 1);
            } else if (orientation == ExifInterface.ORIENTATION_FLIP_VERTICAL) {
                matrix.preScale(1, -1);
            }
            try {
                rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap.
            } catch (OutOfMemoryError outOfMemoryError) {
                rotatedBitmap = null;
            }

        } catch (Exception e) {
            Log.d("RotateImage", e.getMessage());
            rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap.
        }
        return rotatedBitmap;
    }

    public static Uri getImageUri(Context mContext, Bitmap bitmap) {
        if (bitmap != null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap, "Title", null);
            return Uri.parse(path);
        } else {
            return null;
        }
    }

    public static Bitmap getThumbnail(Context mContext, Uri uri) throws FileNotFoundException, IOException {
        InputStream input = mContext.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 400) ? (originalSize / 400) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        input = mContext.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    public static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    public static String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static byte[] getBitmapString(Bitmap bitmap) {
        ByteArrayOutputStream bAOS = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bAOS);
        return bAOS.toByteArray();
    }

    public static Bitmap blurRenderScript(Bitmap smallBitmap, int radius, Context mContext) {

        try {
            if (mContext != null) {


                try {
                    smallBitmap = RGB565toARGB888(smallBitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Bitmap bitmap = Bitmap.createBitmap(
                        smallBitmap.getWidth(), smallBitmap.getHeight(),
                        Bitmap.Config.ARGB_8888);


                RenderScript renderScript = RenderScript.create(mContext);

                Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
                Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

                ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                        Element.U8_4(renderScript));
                blur.setInput(blurInput);
                blur.setRadius(radius); // radius must be 0 < r <= 25
                blur.forEach(blurOutput);

                blurOutput.copyTo(bitmap);
                renderScript.destroy();

                return bitmap;

            } else {
                return null;
            }

        } catch (Exception ex) {
            if (ex.getMessage() != null) {
                Log.e("Exception", ex.getMessage());
                ex.printStackTrace();
            }
            return null;
        } catch (OutOfMemoryError outOfMemoryError) {
            return null;
        }
    }

    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    public static void copyExif(Context context, Uri oldPath, Uri newPath) throws IOException {
        analyzePhotoPathFromLocalUri(context, oldPath);
        ExifInterface oldExif = new ExifInterface(analyzePhotoPathFromLocalUri(context, oldPath));

        String[] attributes = new String[]
                {
                        ExifInterface.TAG_APERTURE,
                        ExifInterface.TAG_DATETIME,
                        ExifInterface.TAG_DATETIME_DIGITIZED,
                        ExifInterface.TAG_EXPOSURE_TIME,
                        ExifInterface.TAG_FLASH,
                        ExifInterface.TAG_FOCAL_LENGTH,
                        ExifInterface.TAG_GPS_ALTITUDE,
                        ExifInterface.TAG_GPS_ALTITUDE_REF,
                        ExifInterface.TAG_GPS_DATESTAMP,
                        ExifInterface.TAG_GPS_LATITUDE,
                        ExifInterface.TAG_GPS_LATITUDE_REF,
                        ExifInterface.TAG_GPS_LONGITUDE,
                        ExifInterface.TAG_GPS_LONGITUDE_REF,
                        ExifInterface.TAG_GPS_PROCESSING_METHOD,
                        ExifInterface.TAG_GPS_TIMESTAMP,
                        ExifInterface.TAG_IMAGE_LENGTH,
                        ExifInterface.TAG_IMAGE_WIDTH,
                        ExifInterface.TAG_ISO,
                        ExifInterface.TAG_MAKE,
                        ExifInterface.TAG_MODEL,
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.TAG_SUBSEC_TIME,
                        ExifInterface.TAG_SUBSEC_TIME_DIG,
                        ExifInterface.TAG_SUBSEC_TIME_ORIG,
                        ExifInterface.TAG_WHITE_BALANCE
                };

        ExifInterface newExif = new ExifInterface(analyzePhotoPathFromLocalUri(context, newPath));
        for (int i = 0; i < attributes.length; i++) {
            String value = oldExif.getAttribute(attributes[i]);
            if (value != null)
                newExif.setAttribute(attributes[i], value);
        }
        newExif.saveAttributes();
    }

    public Bitmap scaleBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        } else {
           /* int width = 2000;
            float scale = (float) width / (float) bitmap.getWidth();
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);*/
            final int REQUIRED_SIZE = 1000;
            int width_tmp = bitmap.getWidth(), height_tmp = bitmap.getHeight();
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
    }
}

