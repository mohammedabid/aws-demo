package com.example.utils


import android.content.Context
import androidx.core.hardware.fingerprint.FingerprintManagerCompat

fun Context.isFingerPrintAvailable(): Boolean {
    return FingerprintManagerCompat.from(this).isHardwareDetected
}

fun Context.hasEnrolledFingerprints(): Boolean {
    return FingerprintManagerCompat.from(this).hasEnrolledFingerprints()
}
