package com.example.utils

import com.example.R

/** The base URL of the API */

const val LANGUAGE_ENGLISH = "en"

object ServiceNames {
    const val AWS_DEMO = "default/dynamodb-writer"
}

object ServiceParams {

    const val SUCCESS_CODE = "0"
    const val FAILURE_CODE = "1"

}

object GlobalConstants {
    const val CONTENT_FRAME_ID = R.id.frame_content
    const val FLAVOR_PROD = "prod"

    const val SERVER_DATE_FORMAT: String = "yyyy-MM-dd hh:mm:ss.SSSSSS"
    const val DATE_FORMAT: String = "dd MMM yyyy hh:MM"
    const val LANG_EN = "en"
    const val LANG_AR = "ar"
}

