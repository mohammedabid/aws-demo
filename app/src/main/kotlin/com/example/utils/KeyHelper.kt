package com.example.utils

import android.content.Context
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import android.util.Log
import com.example.BaseApplication
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.security.Key
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.SecureRandom
import java.util.*
import javax.crypto.*
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.SecretKeySpec
import javax.security.auth.x500.X500Principal

class KeyHelper @Throws(
    Exception::class
)
constructor(ctx: Context) {
    private var keyStore: KeyStore? = null
    private val aesKeyFromKS: Key
        @Throws(
            Exception::class
        )
        get() {
            keyStore = KeyStore.getInstance(AndroidKeyStore)
            keyStore!!.load(null)
            return keyStore!!.getKey(KEY_ALIAS, null) as SecretKey
        }

    init {
        this.generateEncryptKey(ctx)
        this.generateRandomIV(ctx)
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
            try {
                this.generateAESKey(ctx)
            } catch (e: Exception) {
            }

        }
    }


    @Throws(
        Exception::class
    )
    private fun generateEncryptKey(ctx: Context) {
        keyStore = KeyStore.getInstance(AndroidKeyStore)
        keyStore!!.load(null)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (!keyStore!!.containsAlias(KEY_ALIAS)) {
                val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore)
                keyGenerator.init(
                    KeyGenParameterSpec.Builder(
                        KEY_ALIAS,
                        KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                    )
                        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                        .setRandomizedEncryptionRequired(false)
                        .build()
                )
                keyGenerator.generateKey()
            }
        } else {
            if (!keyStore!!.containsAlias(KEY_ALIAS)) {
                // Generate a key pair for encryption
                val start = Calendar.getInstance()
                val end = Calendar.getInstance()
                end.add(Calendar.YEAR, 30)
                val spec = KeyPairGeneratorSpec.Builder(ctx)
                    .setAlias(KEY_ALIAS)
                    .setSubject(X500Principal("CN=$KEY_ALIAS"))
                    .setSerialNumber(BigInteger.TEN)
                    .setStartDate(start.time)
                    .setEndDate(end.time)
                    .build()
                val kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, AndroidKeyStore)
                kpg.initialize(spec)
                kpg.generateKeyPair()
            }
        }


    }

    @Throws(Exception::class)
    private fun rsaEncrypt(secret: ByteArray): ByteArray {
        val privateKeyEntry = keyStore!!.getEntry(KEY_ALIAS, null) as KeyStore.PrivateKeyEntry
        // Encrypt the text
        val inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL")
        inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.certificate.publicKey)
        val outputStream = ByteArrayOutputStream()
        val cipherOutputStream = CipherOutputStream(outputStream, inputCipher)
        cipherOutputStream.write(secret)
        cipherOutputStream.close()

        return outputStream.toByteArray()
    }

    @Throws(Exception::class)
    private fun rsaDecrypt(encrypted: ByteArray): ByteArray {
        val privateKeyEntry = keyStore!!.getEntry(KEY_ALIAS, null) as KeyStore.PrivateKeyEntry
        val output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL")
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.privateKey)
        val cipherInputStream = CipherInputStream(
            ByteArrayInputStream(encrypted), output
        )
        val values = ArrayList<Byte>()
        var nextByte = cipherInputStream.read()
        while (nextByte != -1) {
            values.add(nextByte.toByte())
            nextByte = cipherInputStream.read()
        }

        val bytes = ByteArray(values.size)
        for (i in bytes.indices) {
            bytes[i] = values[i]
        }
        return bytes
    }

    @Throws(Exception::class)
    private fun generateAESKey(context: Context) {
        val pref = context.getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
        var enryptedKeyB64 = pref.getString(ENCRYPTED_KEY, null)
        if (enryptedKeyB64 == null) {
            val key = ByteArray(16)
            val secureRandom = SecureRandom()
            secureRandom.nextBytes(key)
            val encryptedKey = rsaEncrypt(key)
            enryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT)
            val edit = pref.edit()
            edit.putString(ENCRYPTED_KEY, enryptedKeyB64)
            edit.apply()
        }
    }


    @Throws(Exception::class)
    private fun getSecretKey(context: Context): Key {
        val pref = context.getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
        val enryptedKeyB64 = pref.getString(ENCRYPTED_KEY, null)

        val encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT)
        val key = rsaDecrypt(encryptedKey)
        return SecretKeySpec(key, "AES")
    }

    @Throws(
        Exception::class
    )
    fun encrypt(context: Context, input: String): String? {
        val c: Cipher
        val pref = context.getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
        val publicIV = pref.getString(PUBLIC_IV, null)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            c = Cipher.getInstance(AES_MODE_M)
            try {
                c.init(
                    Cipher.ENCRYPT_MODE,
                    aesKeyFromKS,
                    GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT))
                )
                val encodedBytes = c.doFinal(input.toByteArray(charset("UTF-8")))
                return Base64.encodeToString(encodedBytes, Base64.DEFAULT)
            } catch (e: Exception) {
                Log.d(TAG, e.message)
            }

        } else {
            c = Cipher.getInstance(AES_MODE_M)
            try {
                c.init(
                    Cipher.ENCRYPT_MODE,
                    getSecretKey(context),
                    GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT))
                )
                val encodedBytes = c.doFinal(input.toByteArray(charset("UTF-8")))
                return Base64.encodeToString(encodedBytes, Base64.DEFAULT)
            } catch (e: Exception) {
                Log.d(TAG, e.message)
            }

        }
        return ""
    }


    @Throws(
        Exception::class
    )
    fun decrypt(context: Context, encrypted: String?): String {
        val c: Cipher
        val pref = context.getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
        val publicIV = pref.getString(PUBLIC_IV, null)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            c = Cipher.getInstance(AES_MODE_M)
            try {
                c.init(
                    Cipher.DECRYPT_MODE,
                    aesKeyFromKS,
                    GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT))
                )
            } catch (e: Exception) {
            }

        } else {
            c = Cipher.getInstance(AES_MODE_M)
            try {
                c.init(
                    Cipher.DECRYPT_MODE,
                    getSecretKey(context),
                    GCMParameterSpec(128, Base64.decode(publicIV, Base64.DEFAULT))
                )
            } catch (e: Exception) {
            }

        }

        var decodedValue = ByteArray(0)
        try {
            decodedValue = Base64.decode(encrypted!!.toByteArray(charset("UTF-8")), Base64.DEFAULT)
            val decryptedVal = c.doFinal(decodedValue)
            return String(decryptedVal)
        } catch (e: UnsupportedEncodingException) {

        }
        return ""
    }

    fun generateRandomIV(ctx: Context) {
        val pref = ctx.getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
        val publicIV = pref.getString(PUBLIC_IV, null)

        if (publicIV == null) {
            val random = SecureRandom()
            val generated = random.generateSeed(12)
            val generatedIVstr = Base64.encodeToString(generated, Base64.DEFAULT)
            val edit = pref.edit()
            edit.putString(PUBLIC_IV, generatedIVstr)
            edit.apply()
        }
    }

    companion object {
        var TAG = "KEYHELPER"
        private val RSA_MODE = "RSA/ECB/PKCS1Padding"
        private val AES_MODE_M = "AES/GCM/NoPadding"
        private val KEY_ALIAS = "KEY"
        private val AndroidKeyStore = "AndroidKeyStore"
        val SHARED_PREFENCE_NAME = "SAVED_TO_SHARED"
        val ENCRYPTED_KEY = "ENCRYPTED_KEY"
        val PUBLIC_IV = "PUBLIC_IV"
        val ENCRYPTED_PIN = "ENCRYPTED_PIN"
        val ENCRYPTED_PHONE_NUMBER = "ENCRYPTED_PHONE_NUMBER"
        private var keyHelper: KeyHelper? = null

        private val instance: KeyHelper?
            get() {
                if (keyHelper == null) {
                    try {
                        keyHelper = KeyHelper(BaseApplication.applicationContext())
                    } catch (e: java.lang.Exception) {

                    }
                }
                return keyHelper
            }

        fun savePIN(pin: String) {
            try {
                val encryptedPIN = instance!!.encrypt(BaseApplication.applicationContext(), pin)
                if (encryptedPIN != null || !encryptedPIN!!.isEmpty()) {
                    val pref = BaseApplication.applicationContext()
                        .getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
                    pref.edit().putString(ENCRYPTED_PIN, encryptedPIN).apply()
                }
            } catch (e: Exception) {
            }
        }

        val pin: String
            get() {
                val pref =
                    BaseApplication.applicationContext()
                        .getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
                val encryptedString = pref.getString(ENCRYPTED_PIN, "")
                try {
                    val decyprtedString = instance!!.decrypt(BaseApplication.applicationContext(), encryptedString)
                    if (!decyprtedString.isEmpty()) {
                        return decyprtedString
                    }
                } catch (e: java.lang.Exception) {
                    return ""
                }
                return ""
            }

        fun savePhoneNumber(phoneNumber: String) {
            try {
                val encryptedPhoneNumber = instance!!.encrypt(BaseApplication.applicationContext(), phoneNumber)
                if (encryptedPhoneNumber != null || !encryptedPhoneNumber!!.isEmpty()) {
                    val pref = BaseApplication.applicationContext()
                        .getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
                    pref.edit().putString(ENCRYPTED_PHONE_NUMBER, encryptedPhoneNumber).apply()
                }
            } catch (e: Exception) {
            }
        }

        val phoneNumber: String
            get() {
                val pref =
                    BaseApplication.applicationContext()
                        .getSharedPreferences(SHARED_PREFENCE_NAME, Context.MODE_PRIVATE)
                val encryptedString = pref.getString(ENCRYPTED_PHONE_NUMBER, "")
                try {
                    val decyprtedString = instance!!.decrypt(BaseApplication.applicationContext(), encryptedString)
                    if (!decyprtedString.isEmpty()) {
                        return decyprtedString
                    }
                } catch (e: java.lang.Exception) {
                    return ""
                }
                return ""
            }

    }
}
