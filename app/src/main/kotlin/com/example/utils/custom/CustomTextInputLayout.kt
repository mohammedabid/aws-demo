package com.example.utils.custom

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.google.android.material.R
import com.google.android.material.textfield.TextInputLayout

/**
 * Created by hunain.liaquat.
 */
class CustomTextInputLayout(context: Context?, attributeSet: AttributeSet?, defStyleAttr: Int) :
    TextInputLayout(context, attributeSet, defStyleAttr) {

    constructor(context: Context?) : this(context, null) {

    }

    constructor(context: Context?, attributeSet: AttributeSet?) : this(
        context,
        attributeSet,
        R.attr.textInputStyle
    ) {

    }

    override fun drawableStateChanged() {
        super.drawableStateChanged()
        clearEditTextColorfilter()
    }

    override fun setError(@Nullable error: CharSequence?) {
        super.setError(error)
        clearEditTextColorfilter()
    }

    private fun clearEditTextColorfilter() {
        val editText = editText
        if (editText != null) {
            val background = editText.background
            background?.clearColorFilter()
        }
    }
}