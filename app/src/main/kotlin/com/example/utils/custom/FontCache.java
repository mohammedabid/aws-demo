package com.example.utils.custom;

import android.content.Context;
import android.graphics.Typeface;
import androidx.core.content.res.ResourcesCompat;

import java.util.HashMap;

public class FontCache {

    private static HashMap<String, Typeface> fontCache = new HashMap<>();

    public static Typeface getTypeface(String fontname, Context context) {
        Typeface typeface = fontCache.get(fontname);

        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), fontname);
            } catch (Exception e) {
                return null;
            }

            fontCache.put(fontname, typeface);
        }

        return typeface;
    }

    public static Typeface getTypefaceFromFonts(int fontId, Context context) {
        Typeface typeface = fontCache.get(String.valueOf(fontId));

        if (typeface == null) {
            try {
                typeface = ResourcesCompat.getFont(context, fontId);

            } catch (Exception e) {
                return null;
            }

            fontCache.put(String.valueOf(fontId), typeface);
        }

        return typeface;
    }
}