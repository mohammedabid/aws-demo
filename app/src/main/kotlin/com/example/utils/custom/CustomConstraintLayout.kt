package com.example.utils.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import androidx.annotation.AttrRes

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.animation.PathInterpolatorCompat
import kotlin.math.roundToLong


/**
 * Created by hunain.liaquat.
 */
//class CustomConstraintLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {
//
//    var animationDuration: Int = 0
//    private val isVertical: Boolean
//    private var expansion: Float = 0.toFloat()
//    private val displacement: Float
//    var interpolator: TimeInterpolator? = null
//    private var animationListener: CustomConstraintLayoutListener? = null
//    private var valueAnimator: ValueAnimator? = null
//    private var currentStatus = ExpandableConstraintLayoutStatus.IDLE
//
//    val isExpanded: Boolean
//        get() = expansion == 1f
//
//    init {
//        // default values
//        animationDuration = 200
//        interpolator = FastOutSlowInInterpolator()
//        isVertical = true
//        displacement = 1f
//        expansion = 1f
//    }
//
//    // Overridden Methods
//    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
//        val width = measuredWidth
//        val height = measuredHeight
//        val size = if (isVertical) height else width
//        visibility = if (expansion == 0f && size == 0) View.INVISIBLE else View.VISIBLE
//        val expansionDelta = size - Math.round(size * expansion)
//        // translate all children before measuring the parent
//        if (displacement > 0) {
//            val displacementDelta = expansionDelta * displacement
//            for (i in 0 until childCount) {
//                if (isVertical) {
//                    getChildAt(i).translationY = -displacementDelta
//                } else {
//                    val direction = -1
//                    getChildAt(i).translationX = direction * displacementDelta
//                }
//            }
//        }
//        if (isVertical) {
//            setMeasuredDimension(width, height - expansionDelta)
//        } else {
//            setMeasuredDimension(width - expansionDelta, height)
//        }
//    }
//
//    // General Methods
//    fun toggle() {
//        if (isExpanded) {
//            collapse()
//        } else {
//            expand()
//        }
//    }
//
//    fun expand() {
//        shouldExpand(true, true)
//    }
//
//    fun collapse() {
//        shouldExpand(false, true)
//    }
//
//    private fun shouldExpand(shouldExpand: Boolean, shouldAnimate: Boolean) {
//        if (shouldExpand && (currentStatus == ExpandableConstraintLayoutStatus.EXPANDING || expansion == 1f)) {
//            return
//        }
//        if (!shouldExpand && (currentStatus == ExpandableConstraintLayoutStatus.COLLAPSING || expansion == 0f)) {
//            return
//        }
//        val newExpansion = if (shouldExpand) 1F else 0F
//        if (shouldAnimate) {
//            animateExpansion(newExpansion)
//        } else {
//            setExpansion(newExpansion)
//        }
//    }
//
//    private fun setExpansion(newExpansion: Float) {
//        // Nothing to do here
//        if (this.expansion == newExpansion) {
//            return
//        }
//        this.expansion = newExpansion
//        visibility = if (expansion == 0f) View.INVISIBLE else View.VISIBLE
//        requestLayout()
//    }
//
//    private fun animateExpansion(newExpansion: Float) {
//        if (valueAnimator != null) {
//            valueAnimator?.cancel()
//            valueAnimator = null
//        }
//        valueAnimator = ValueAnimator.ofFloat(expansion, newExpansion)
//        valueAnimator?.interpolator = interpolator
//        valueAnimator?.duration = animationDuration.toLong()
//        valueAnimator?.addUpdateListener { valueAnimator -> setExpansion(valueAnimator.animatedValue as Float) }
//        valueAnimator?.addListener(object : Animator.AnimatorListener {
//            override fun onAnimationStart(animation: Animator) {
//                reportListenerStatus(ExpandableConstraintLayoutListenerStatus.AnimationStart)
//                currentStatus = if (newExpansion <= 0) {
//                    reportListenerStatus(ExpandableConstraintLayoutListenerStatus.PreClose)
//                    ExpandableConstraintLayoutStatus.COLLAPSING
//                } else {
//                    reportListenerStatus(ExpandableConstraintLayoutListenerStatus.PreOpen)
//                    ExpandableConstraintLayoutStatus.EXPANDING
//                }
//            }
//
//            override fun onAnimationEnd(animation: Animator) {
//                reportListenerStatus(ExpandableConstraintLayoutListenerStatus.AnimationEnd)
//                if (currentStatus == ExpandableConstraintLayoutStatus.EXPANDING) {
//                    reportListenerStatus(ExpandableConstraintLayoutListenerStatus.Opened)
//                } else {
//                    reportListenerStatus(ExpandableConstraintLayoutListenerStatus.Closed)
//                }
//                currentStatus = ExpandableConstraintLayoutStatus.IDLE
//            }
//
//            override fun onAnimationCancel(animation: Animator) {
//                currentStatus = ExpandableConstraintLayoutStatus.IDLE
//            }
//
//            override fun onAnimationRepeat(animation: Animator) {}
//        })
//        valueAnimator?.start()
//    }
//
//    // Listener Related
//    fun setAnimationListener(listener: CustomConstraintLayoutListener) {
//        this.animationListener = listener
//    }
//
//    fun reportListenerStatus(status: ExpandableConstraintLayoutListenerStatus?) {
//        if (status != null && animationListener != null) {
//            when (status) {
//                ExpandableConstraintLayoutListenerStatus.PreOpen -> animationListener!!.onPreOpen()
//                ExpandableConstraintLayoutListenerStatus.PreClose -> animationListener!!.onPreClose()
//                ExpandableConstraintLayoutListenerStatus.Opened -> animationListener!!.onOpened()
//                ExpandableConstraintLayoutListenerStatus.Closed -> animationListener!!.onClosed()
//                ExpandableConstraintLayoutListenerStatus.AnimationStart -> animationListener!!.onAnimationStart(currentStatus)
//                ExpandableConstraintLayoutListenerStatus.AnimationEnd -> animationListener!!.onAnimationEnd(currentStatus)
//                else -> {
//                }
//            }
//        }
//    }
//
//}

// Constructors

class CustomConstraintLayout: ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(
        context: Context, attrs: AttributeSet?,
        @AttrRes defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)

    var easeInOutQuart = PathInterpolatorCompat.create(0.77f, 0f, 0.175f, 1f)

    fun expand(view: View) {
        val matchParentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec((view.parent as View).width, View.MeasureSpec.EXACTLY)
        val wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec((view.parent as View).height, View.MeasureSpec.UNSPECIFIED)
        view.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = view.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0 so use 1 instead.
        view.layoutParams.height = 1

        val animation = object : Animation() {


            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {

                view.layoutParams.height = if (interpolatedTime == 1f)
                    ViewGroup.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()

                view.requestLayout()
            }


            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        animation.interpolator = easeInOutQuart
        val collapselListener = object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
            }

            override fun onAnimationRepeat(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                view.visibility = View.VISIBLE
            }
        }
        animation.setAnimationListener(collapselListener)
        val durationMillis = computeDurationFromHeight(view)
        animation.duration = 750
        view.startAnimation(animation)

    }

    fun collapse(view: View) {
        val initialHeight = view.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    view.visibility = View.GONE
                } else {
                    view.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    view.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.interpolator = easeInOutQuart
        val durationMillis = computeDurationFromHeight(view)
        a.duration = 750
        view.startAnimation(a)

    }

    private fun computeDurationFromHeight(view: View): Long {
        // 1dp/ms * multiplier
        return (view.measuredHeight / view.context.resources.displayMetrics.density).roundToLong()
    }
}