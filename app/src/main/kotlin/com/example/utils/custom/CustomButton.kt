package com.example.utils.custom

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.R
import androidx.appcompat.widget.AppCompatButton

/**
 * Created by hunain.liaquat.
 */
class CustomButton : AppCompatButton {
    constructor(context: Context?, attributeSet: AttributeSet?, defStyleAttr: Int) :
            super(context, attributeSet, defStyleAttr){
    }

    constructor(context: Context?) : this(context, null) {

    }

    constructor(context: Context?, attributeSet: AttributeSet?) : this(
        context,
        attributeSet,
        R.attr.buttonStyle
    ) {

    }
}