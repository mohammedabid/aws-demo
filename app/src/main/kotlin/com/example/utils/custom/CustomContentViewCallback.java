package com.example.utils.custom;

import android.view.View;
import androidx.core.view.ViewCompat;
import com.google.android.material.snackbar.ContentViewCallback;

class CustomContentViewCallback implements ContentViewCallback {

        // view inflated from custom layout
        private View content;

        public CustomContentViewCallback(View content) {
            this.content = content;
        }

        @Override
        public void animateContentIn(int delay, int duration) {
            // add custom *in animations for your views
            // e.g. original snackbar uses alpha animation, from 0 to 1
            content.setScaleY(0f);
            ViewCompat.animate(content)
                    .scaleY(1f).setDuration(duration)
                    .setStartDelay(delay);
        }

        @Override
        public void animateContentOut(int delay, int duration) {
            // add custom *out animations for your views
            // e.g. original snackbar uses alpha animation, from 1 to 0
            content.setScaleY(1f);
            ViewCompat.animate(content)
                    .scaleY(0f)
                    .setDuration(duration)
                    .setStartDelay(delay);
        }
    }