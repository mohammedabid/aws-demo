package com.example.utils.custom

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.Animation
import android.view.animation.Transformation
import androidx.appcompat.R
import androidx.cardview.widget.CardView


/**
 * Created by hunain.liaquat.
 */
class CustomCardView : CardView {
    constructor(context: Context?, attributeSet: AttributeSet?, defStyleAttr: Int) :
            super(context!!, attributeSet, defStyleAttr) {
    }

    constructor(context: Context?) : this(context, null) {

    }

    constructor(context: Context?, attributeSet: AttributeSet?) : this(
        context,
        attributeSet,
        R.attr.buttonStyle
    ) {

    }


    fun expand() {
        (getChildAt(0) as ViewGroup).getChildAt(2).viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {

                Log.d("height", "height " + (getChildAt(0) as ViewGroup).getChildAt(2).height)
            }
        }
        )
        val initialHeight = height
        measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        val targetHeight =
            measuredHeight + (getChildAt(0) as ViewGroup).getChildAt(
                2
            ).height

        val distanceToExpand = targetHeight - initialHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    // Do this after expanded
//                    findViewById<ConstraintLayout>(com.indy.R.id.cl_recharge_card).visibility = View.VISIBLE
//                    visibility = View.VISIBLE
                }

                layoutParams.height = (initialHeight + distanceToExpand * interpolatedTime).toInt()
                requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = 1000
        startAnimation(a)
    }

    fun collapse() {
        val initialHeight = measuredHeight
        var collapsedHeight =
            measuredHeight - (getChildAt(0) as ViewGroup).getChildAt(
                2
            ).height

        val distanceToCollapse = initialHeight - collapsedHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    // Do this after collapsed
//                    findViewById<ConstraintLayout>(com.indy.R.id.cl_recharge_card).visibility = View.GONE
                }

                layoutParams.height = (initialHeight - distanceToCollapse * interpolatedTime).toInt()
                requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = distanceToCollapse.toLong()
        startAnimation(a)
    }
}