package com.example.utils.custom

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 * Created by hunain.liaquat.
 */
class CustomTextView : AppCompatTextView {

    constructor(context: Context?, attributeSet: AttributeSet?, defStyleAttr: Int) :
            super(context, attributeSet, defStyleAttr) {
    }

    constructor(context: Context?) : this(context, null) {

    }

    constructor(context: Context?, attributeSet: AttributeSet?) : this(
        context,
        attributeSet,
        android.R.attr.textViewStyle
    ) {

    }
}