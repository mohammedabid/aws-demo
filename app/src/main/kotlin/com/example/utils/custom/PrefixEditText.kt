package com.example.utils.custom

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import com.example.R

class PrefixEditText : AppCompatEditText {

    private var mOriginalLeftPadding = -1f
    private var hintColor: Int = ContextCompat.getColor(context, R.color.indicator_unselected)

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(
        context: Context, attrs: AttributeSet,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
    }

    override fun onMeasure(
        widthMeasureSpec: Int,
        heightMeasureSpec: Int
    ) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        calculatePrefix()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val prefix = tag as String
        paint.color = hintColor
        canvas.drawText(
            prefix, mOriginalLeftPadding,
            getLineBounds(0, null).toFloat(), paint
        )
        paint.color = ContextCompat.getColor(context, R.color.indicator_selected)
    }

    fun setHintColor(color: Int) {
        this.hintColor = color
        invalidate()
    }

    private fun calculatePrefix() {
        if (mOriginalLeftPadding == -1f) {
            val prefix = tag as String
            val widths = FloatArray(prefix.length)
            paint.getTextWidths(prefix, widths)
            var textWidth = 0f
            for (w in widths) {
                textWidth += w
            }
            mOriginalLeftPadding = compoundPaddingLeft.toFloat()
            setPadding(
                (textWidth + mOriginalLeftPadding).toInt(),
                paddingRight, paddingTop, paddingBottom
            )
        }
    }
}