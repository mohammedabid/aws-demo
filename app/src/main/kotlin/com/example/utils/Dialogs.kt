package com.example.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.WindowManager
import com.example.R


object Dialogs {

    fun showInviteFriendDialog(
        context: Context, title: String, message: String, invitationCode: String, buttonText: String,
        cancelable: Boolean?, runnable: Runnable?
    ): Dialog {

        val dialog = Dialog(context, R.style.AppTheme)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.WHITE))


        val lp = dialog.window!!.attributes
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT

        dialog.window!!.attributes = lp
        dialog.setCancelable(cancelable!!)
        return dialog
    }


}