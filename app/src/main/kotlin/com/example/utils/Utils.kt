package com.example.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.pm.PackageInfoCompat
import androidx.fragment.app.Fragment
import com.example.repository.data_sources.PreferenceKeys.PREF_UNIQUE_ID
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToLong

fun Fragment.hideKeyboard(view: View?) {
    activity!!.hideKeyboard(view)
}

fun Activity.hideKeyboard() {
    hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
}

fun Context.hideKeyboard(view: View?) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view!!.windowToken, 0)
}

@Synchronized
fun getDeviceId(context: Context): String {
    var uniqueID: String? = null
    if (uniqueID == null) {
        val sharedPrefs = context.getSharedPreferences(
            PREF_UNIQUE_ID, Context.MODE_PRIVATE
        )
        uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)
        if (uniqueID == null) {
            uniqueID = UUID.randomUUID().toString()
            val editor = sharedPrefs.edit()
            editor.putString(PREF_UNIQUE_ID, uniqueID)
            editor.commit()
        }
    }
    return uniqueID
}

@Synchronized
fun getVersionCode(context: Context): Long {
    var versionCode: Long = 0
    try {
        val pInfo = context.packageManager.getPackageInfo(
            context.packageName,
            0
        )
        versionCode = PackageInfoCompat.getLongVersionCode(pInfo)
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return versionCode
}

@Synchronized
fun getVersionName(context: Context): String {
    var versionName = ""
    try {
        val pInfo = context.packageManager.getPackageInfo(
            context.packageName,
            0
        )
        versionName = pInfo.versionName
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return versionName
}

fun round(value: Double, places: Int): Double {
    var value = value
    if (places < 0) throw IllegalArgumentException()

    val factor = Math.pow(10.0, places.toDouble()).toLong()
    value *= factor
    val tmp = value.roundToLong()
    return tmp.toDouble() / factor
}

fun convertDpToPixels(context: Context, dp: Float): Float {
    return dp * context.resources.displayMetrics.density
}