package com.example.injection

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.example.BuildConfig
import com.example.base.AppExecutors
import com.example.base.Navigator
import com.example.model.base.BaseRequestModel
import com.example.repository.ApiInterface
import com.example.repository.data_sources.LocalDataSource
import com.example.repository.data_sources.NetworkDataSource
import com.example.repository.data_sources.SharedPrefsDataSource
import com.example.repository.database.AppDatabase
import com.example.repository.repos.ContentRepo
import com.example.utils.GlobalConstants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .readTimeout(300, TimeUnit.SECONDS)
            .connectTimeout(300, TimeUnit.SECONDS)
            .addInterceptor(interceptor).addInterceptor { chain ->
                val original = chain.request()
                var request: Request
                if (BuildConfig.FLAVOR.equals(GlobalConstants.FLAVOR_PROD, ignoreCase = true)) {
                    request = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build()
                } else {
                    request = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build()
                }
                chain.proceed(request!!)
            }.build()
    }

    @Singleton
    @Provides
    fun provideApiInterface(client: OkHttpClient): ApiInterface {

        return Retrofit.Builder()
            .baseUrl(BuildConfig.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
            .create(ApiInterface::class.java)
    }

    @Singleton
    @Provides
    fun provideNetworkDataSource(
        apiInterface: ApiInterface
    ): NetworkDataSource {
        return NetworkDataSource(apiInterface)
    }

    @Singleton
    @Provides
    fun provideLocalDataSource(appDatabase: AppDatabase): LocalDataSource {
        return LocalDataSource(appDatabase)
    }

    @Singleton
    @Provides
    fun provideSharedPrefDataSource(@AppContext context: Context): SharedPrefsDataSource {
        return SharedPrefsDataSource(context)
    }

    @Provides
    fun provideBaseRequest(
        application: Application
    ): BaseRequestModel {
        return BaseRequestModel(application)
    }

    @Provides
    fun provideContentRepo(
        networkDataSource: NetworkDataSource, localDataSource: LocalDataSource,
        sharedPrefsDataSource: SharedPrefsDataSource,
        baseRequestModel: BaseRequestModel
    ): ContentRepo {

//        return if (BuildConfig.FLAVOR.equals(GlobalConstants.FLAVOR_MOCK, ignoreCase = true)) {
//            ContentRepo(localDataSource, sharedPrefsDataSource,baseRequestModel )
//        } else {
//            ContentRepo(networkDataSource, sharedPrefsDataSource,baseRequestModel )
//        }
            return ContentRepo(networkDataSource, sharedPrefsDataSource,baseRequestModel )
//        }
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDatabase {
        return Room
            .databaseBuilder(app, AppDatabase::class.java, "local.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    @AppContext
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    fun provideAppExecutor(): AppExecutors {
        return AppExecutors()
    }


    @Provides
    fun provideNavigator(

        localDataSource: LocalDataSource,
        sharedPrefDataSource: SharedPrefsDataSource
    ): Navigator {
        return Navigator(localDataSource, sharedPrefDataSource)
    }
}