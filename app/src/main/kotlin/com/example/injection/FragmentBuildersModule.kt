package com.example.injection

import com.example.ui.awsdemo.fragment.AWSDemoFragment
import com.example.ui.awsdemo.fragment.AWSDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeAWSDemoFragment(): AWSDemoFragment

    @ContributesAndroidInjector
    abstract fun contributeAWSDetailFragment(): AWSDetailFragment

}
//