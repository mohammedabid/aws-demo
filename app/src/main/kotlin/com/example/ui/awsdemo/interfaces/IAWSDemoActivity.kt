package com.example.ui.awsdemo.interfaces

import com.example.model.awsdemo.AWSResult


interface IAWSDemoActivity{
     fun openDetail(awsResult: AWSResult)
}