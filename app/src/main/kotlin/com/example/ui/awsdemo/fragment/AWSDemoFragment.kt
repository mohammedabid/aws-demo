package com.example.ui.awsdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import androidx.lifecycle.Observer
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.R
import com.example.base.*
import com.example.databinding.FragmentAwsDemoBinding
import com.example.model.awsdemo.AWSResult
import com.example.repository.data_sources.SharedPrefsDataSource
import com.example.ui.awsdemo.adapters.AWSDemoAdapter
import com.example.ui.awsdemo.viewmodel.AWSDemoViewModel
import com.example.ui.awsdemo.interfaces.IAWSDemoActivity
import javax.inject.Inject

class AWSDemoFragment : BaseFragment(), IAWSDemoActivity {

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPrefsDataSource: SharedPrefsDataSource

    lateinit var binding: FragmentAwsDemoBinding

    lateinit var awsDemoViewModel: AWSDemoViewModel
    private var awsResultList = ArrayList<AWSResult>()
    lateinit var notificationsInboxAdapter: AWSDemoAdapter

    @Inject
    lateinit var appExecutors: AppExecutors

    var dataBindingComponent = MyDataBindingComponent(this)

    private val TAG = "AWSDemoFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_aws_demo, container, false, dataBindingComponent
        )
        rootView = binding.root
        return rootView
    }

    override fun initUI() {
        if (activity is BaseActivity) {
//            (activity as BaseActivity).setToolbar(R.layout.layout_toolbar_dark)
//            (activity as BaseActivity).setBackButtonDrawable(R.drawable.ic_icon_arrow_left_white)
//            (activity as BaseActivity).showToolbar(true)
//            (activity as BaseActivity).setToolbarTitle(getString(R.string.notifications))
        }

        notificationsInboxAdapter =
            AWSDemoAdapter(Glide.with(this), this, dataBindingComponent, appExecutors)
        binding.rvAwsDemo.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        binding.rvAwsDemo.adapter = notificationsInboxAdapter

        awsDemoViewModel.getAWSDemoLiveData()

        awsDemoViewModel.awsDemoLiveData.removeObservers(this)
        awsDemoViewModel.awsDemoLiveData.observe(this,
            Observer {
                if (it != null) {
                    if (!it?.results.isNullOrEmpty()) {
                        binding.tvEmptyList.visibility = View.GONE
                        awsResultList.clear()
                        it?.results?.let { results ->
                            awsResultList.addAll(results)
                        }
                    } else {
                        binding.tvEmptyList.visibility = View.VISIBLE
                    }
                    notificationsInboxAdapter.submitList(awsResultList)
                }
            })

    }

    override fun initViewModel(): BaseViewModel? {
        awsDemoViewModel = ViewModelProvider(this, viewModelFactory)
            .get(AWSDemoViewModel::class.java)

        return awsDemoViewModel
    }

    override fun openDetail(awsResult: AWSResult) {
            var bundle = Bundle()
            bundle.putParcelable("awsResult", awsResult)
            navigator.gotoNotificationsDetailsActivity(activity as BaseActivity, bundle)
    }


}