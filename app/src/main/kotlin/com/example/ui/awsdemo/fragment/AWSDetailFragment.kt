package com.example.ui.awsdemo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.R
import com.example.base.BaseFragment
import com.example.base.BaseViewModel
import com.example.base.MyDataBindingComponent
import com.example.databinding.FragmentAwsDetailBinding
import com.example.model.awsdemo.AWSResult

class AWSDetailFragment : BaseFragment() {

    lateinit var fragmentAwsDetailBinding: FragmentAwsDetailBinding
    var dataBindingComponent = MyDataBindingComponent(this)
    var awsResult: AWSResult? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentAwsDetailBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_aws_detail, container, false, dataBindingComponent
        )
        rootView = fragmentAwsDetailBinding.root
        return rootView
    }

    override fun initUI() {
        awsResult = arguments!!["awsResult"] as AWSResult
        fragmentAwsDetailBinding!!.awsItem = awsResult

        if(awsResult?.imageUrls!!.isNotEmpty() && awsResult?.imageUrls!![0].isNotEmpty()){
            fragmentAwsDetailBinding.ivBanner.visibility = View.VISIBLE

            val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
            Glide.with(this).setDefaultRequestOptions(requestOptions)
                .load(awsResult!!.imageUrls[0])
                .into(fragmentAwsDetailBinding.ivBanner)
        }else {
            fragmentAwsDetailBinding.ivBanner.visibility = View.GONE
        }
    }

    override fun initViewModel(): BaseViewModel? {
        return null
    }
}