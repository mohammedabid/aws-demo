package com.example.ui.awsdemo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Priority
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.R
import com.example.base.AppExecutors
import com.example.base.DataBoundListAdapter
import com.example.databinding.ItemAwsDemoBinding
import com.example.model.awsdemo.AWSResult
import com.example.ui.awsdemo.fragment.AWSDemoFragment

class AWSDemoAdapter(
    private val glide: RequestManager,
    private val awsDemoFragment: AWSDemoFragment,
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors
) : DataBoundListAdapter<AWSResult, ItemAwsDemoBinding>(
appExecutors = appExecutors,
diffCallback = categoryListDiffCallback
) {

    override fun createBinding(parent: ViewGroup): ItemAwsDemoBinding {
        val binding = DataBindingUtil.inflate<ItemAwsDemoBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_aws_demo,
            parent,
            false,
            dataBindingComponent
        )

        return binding
    }

    override fun bind(binding: ItemAwsDemoBinding, item: AWSResult, position: Int) {
        binding.awsItem = item

        if(item.imageUrls.isNotEmpty() && !item.imageUrls[0].isNullOrEmpty()){
            binding.ivBanner.visibility = View.VISIBLE

            val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
            glide.setDefaultRequestOptions(requestOptions)
                .load(item.imageUrls[0])
                .into(binding.ivBanner)
        }else {
            binding.ivBanner.visibility = View.GONE
        }

        binding.cvNotificationItem.setOnClickListener {
            awsDemoFragment.openDetail(binding.awsItem!!)
        }

    }
}

val categoryListDiffCallback: DiffUtil.ItemCallback<AWSResult> =
    object : DiffUtil.ItemCallback<AWSResult>() {
        override fun areItemsTheSame(oldItem: AWSResult, newItem: AWSResult): Boolean {
            return (oldItem.uid == newItem.uid)
        }

        override fun areContentsTheSame(oldItem: AWSResult, newItem: AWSResult): Boolean {
            return (oldItem.uid == newItem.uid)
        }
    }