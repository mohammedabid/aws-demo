package com.example.ui.awsdemo.viewmodel

import androidx.lifecycle.MutableLiveData
import com.example.base.BaseViewModel
import com.example.base.Resource
import com.example.model.awsdemo.GetAWSDemoResponseModel
import com.example.repository.repos.ContentRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class AWSDemoViewModel @Inject constructor(var contentRepo: ContentRepo) : BaseViewModel() {
    var awsDemoLiveData = MutableLiveData<GetAWSDemoResponseModel>()

    fun getAWSDemoLiveData(): MutableLiveData<GetAWSDemoResponseModel> {
        mCompositeDisposable.add(
            contentRepo.getAWSDemo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    //show loader
                }.doOnComplete {
                    //hide loader

                }
                .subscribe({ baseResponse ->
                    run {
                         awsDemoLiveData.value = baseResponse
                    }
                }, {
                    run {
                        stateLiveData.value = Resource.error(it)
                    }
                })
        )
        return awsDemoLiveData
    }

}