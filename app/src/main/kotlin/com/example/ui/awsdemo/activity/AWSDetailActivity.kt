package com.example.ui.awsdemo.activity


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.R
import com.example.base.BaseActivity
import com.example.base.BaseFragment
import com.example.ui.awsdemo.fragment.AWSDetailFragment
import com.example.utils.GlobalConstants.CONTENT_FRAME_ID
import javax.inject.Inject

class AWSDetailActivity : BaseActivity() {

    var TAG: String = "AWSDetailActivity"
    var fragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frame)

        fragment = supportFragmentManager.findFragmentByTag(TAG)
        if (fragment == null) {
            fragment = AWSDetailFragment()
        }
        fragment!!.arguments = intent.extras
        replaceFragment(fragment as BaseFragment, false, CONTENT_FRAME_ID, TAG)
    }

}