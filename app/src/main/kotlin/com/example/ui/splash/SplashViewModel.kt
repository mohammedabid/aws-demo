package com.example.ui.splash

import androidx.lifecycle.MutableLiveData
import com.example.base.BaseViewModel
import com.example.base.Resource
import com.example.model.awsdemo.GetAWSDemoResponseModel
import com.example.repository.repos.ContentRepo
import com.example.utils.ServiceParams
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SplashViewModel @Inject constructor(var contentRepo: ContentRepo) : BaseViewModel() {
    var appUpdateLiveData = MutableLiveData<GetAWSDemoResponseModel>()
    var resourceAppConfigLiveData = MutableLiveData<GetAWSDemoResponseModel>()
    var loginSuccessLiveData = MutableLiveData<String>()

}