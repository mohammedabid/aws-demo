package com.example.ui.splash


import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.R
import com.example.base.BaseActivity
import com.example.databinding.ActivitySplashBinding
import javax.inject.Inject

class SplashActivity : BaseActivity(), ISplashActivity {
//    var TAG: String? = SplashActivity::class.qualifiedName

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var splashViewModel: SplashViewModel
    lateinit var activitySplashBinding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySplashBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.activity_splash, null, false
        )
        setContentView(R.layout.activity_splash)
        val decorView = window.decorView
        // Hide the status bar.
        val uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN
        decorView.systemUiVisibility = uiOptions
        showToolbar(false)
        splashViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(SplashViewModel::class.java)

        observeLiveData()
4
    }

    fun observeLiveData() {

    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed(
            {
                initUI()
            }
            , 2000)
    }

    private fun initUI() {
        navigator.gotoAWSDemoActivity(this, Bundle())
    }


}