package com.example

import android.app.Activity
import android.app.Application
import android.content.Context
import com.example.injection.AppInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class BaseApplication : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector


    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
    }

    init {
        instance = this
    }

    companion object {
        private var instance: BaseApplication? = null
        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

}