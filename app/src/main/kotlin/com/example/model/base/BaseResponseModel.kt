package com.example.model.base

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class BaseResponseModel<T>(
    @SerializedName("responseCode")
    var responseCode: String,
    @SerializedName("responseMessage")
    var responseMessage: String,
    @SerializedName("data")
    var data: T? = null
)