package com.example.model.base

import android.app.Application
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseRequestModel(baseRequest: BaseRequestModel?) {
    @SerializedName("appVersion")
    @Expose
    private var appVersion: String? = null
    @SerializedName("buildNumber")
    @Expose
    private var buildNumber: Long? = null
    @SerializedName("msisdn")
    @Expose
    open var msisdn: String? = null
    @SerializedName("token")
    @Expose
    private var token: String? = null
    @SerializedName("osVersion")
    @Expose
    private var osVersion: String? = null
    @SerializedName("osType")
    @Expose
    private var osType: String? = null
    @SerializedName("channel")
    @Expose
    private var channel: String? = null
    @SerializedName("lang")
    @Expose
    open var lang: String? = null
    @SerializedName("imsi")
    @Expose
    private var imsi: String? = null
    @SerializedName("deviceId")
    @Expose
    private var deviceId: String? = null
    @SerializedName("authToken")
    @Expose
    private var authToken: String? = null
    @SerializedName("registrationId")
    @Expose
    open var registrationId: String? = null

    init {
        setBaseRequestModel(baseRequest)
    }

    fun setBaseRequestModel(baseRequest: BaseRequestModel?) {
        if (baseRequest != null) {
            if (!baseRequest.authToken.isNullOrEmpty())
                authToken = baseRequest.authToken
            if (!baseRequest.token.isNullOrEmpty())
                token = baseRequest.token
            if (!baseRequest.registrationId.isNullOrEmpty())
                registrationId = baseRequest.registrationId
            lang = baseRequest.lang
            imsi = baseRequest.imsi
            osVersion = baseRequest.osVersion
            osType = baseRequest.osType
            channel = baseRequest.channel
            if (!baseRequest.msisdn.isNullOrEmpty()) {
                msisdn = baseRequest.msisdn
            }
            deviceId = baseRequest.deviceId
            appVersion = baseRequest.appVersion
            buildNumber = baseRequest.buildNumber
        }
    }

    constructor(application: Application) : this(null) {
        osVersion = android.os.Build.VERSION.SDK_INT.toString()
    }

}