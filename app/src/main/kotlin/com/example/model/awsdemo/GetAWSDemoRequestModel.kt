package com.example.model.awsdemo

import com.google.gson.annotations.SerializedName
import com.example.model.base.BaseRequestModel

class GetAWSDemoRequestModel (baseRequest: BaseRequestModel,
                              @SerializedName("significant")
                          var significant: String
) : BaseRequestModel(baseRequest)