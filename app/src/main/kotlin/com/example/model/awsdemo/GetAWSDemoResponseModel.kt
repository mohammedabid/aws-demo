package com.example.model.awsdemo

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class GetAWSDemoResponseModel(
    @SerializedName("pagination")
    var pagination: Pagination,
    @SerializedName("results")
    var results: List<AWSResult>
) : Parcelable

@Parcelize
data class Pagination(
    @SerializedName("key")
    var key: String
) : Parcelable


//@Entity
@Parcelize
data class AWSResult(
    @SerializedName("created_at")
    var createdAt: String,
    @SerializedName("image_ids")
    var imageIds: List<String>,
    @SerializedName("image_urls")
    var imageUrls: List<String>,
    @SerializedName("image_urls_thumbnails")
    var imageUrlsThumbnails: List<String>,
    @SerializedName("name")
    var name: String,
    @SerializedName("price")
    var price: String,
    @SerializedName("uid")
    var uid: String

) : Parcelable
